
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }, 
      { path: 'anomie', component: () => import('pages/Anomie.vue') },
      { path: 'chaos', component: () => import('pages/Chaos.vue') },
      { path: 'insurrection', component: () => import('pages/Insurrection.vue') },
      { path: 'lawlessness', component: () => import('pages/Lawlessness.vue') },
      { path: 'misrule', component: () => import('pages/Misrule.vue') },
      { path: 'mobocracy', component: () => import('pages/Mobocracy.vue') },
      { path: 'mutiny', component: () => import('pages/Mutiny.vue') },
      { path: 'nihilism', component: () => import('pages/Nihilism.vue') },
      { path: 'pandemonium', component: () => import('pages/Pandemonium.vue') },
      { path: 'rebellion', component: () => import('pages/Rebellion.vue') },
      { path: 'tumult', component: () => import('pages/Tumult.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
